#include <stdio.h>

int main(){
    
    int szam;
    
    printf("Adjon meg egy decimális számot: ");
    scanf("%d", &szam);
    printf("Az szám unáris számrendszerben: ");

    for(int i = 0; i < szam; i++)
        printf("1");

    printf("\n");

    return 0;
}


#include <stdio.h>
#include <stdlib.h>

int main()
{
	//1
	printf("Változócsere segédváltozóval:\n");
	int a = 3;
	int b = 4;

	printf("Kiindulási adatok: a= %d ; b= %d \n", a,b);

	int tmp = a; 
	a = b; 
	b = tmp;

	printf("Eredmény: a= %d ; b= %d \n", a,b);


	//2
	printf("Változócsere segédváltozó nélkül:\n");

	a = 3;		
	b = 4;
	
	printf("Kiindulási adatok: a= %d ; b= %d \n", a,b);

	a= a + b;
	b= a - b;
	a= a - b;

	printf("Kiindulási adatok: a= %d ; b= %d \n", a,b);


	//3
	printf("Változócsere EXOR-al:\n");

	a = 3;		
	b = 4;

	printf("Kiindulási adatok: a= %d ; b= %d \n", a,b);

	a = a^b;
	b = a^b;
	a = a^b;

	printf("Kiindulási adatok: a= %d ; b= %d \n", a,b);

	return 0;


}

#include <stdio.h>

int main()
{
	int szam = 5;
	printf("A szám decimálisan: %d\n", szam);
	printf("A szám unárisan:\n");

	for(int i; i<szam; i++)
		printf("1");
	
	printf("\n");

	return 0;
}
